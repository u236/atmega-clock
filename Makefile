CC = /usr/local/CrossPack-AVR/bin/avr-gcc
OC = /usr/local/CrossPack-AVR/bin/avr-objcopy
AD = /usr/local/Cellar/avrdude/6.3_1/bin/avrdude

TGT = clock
MCU = atmega88pa
CLK = 8000000
SRC = i2c.c ds1307.c ws2812.c main.c
OBJ = $(SRC:.c=.o)

PART = m88p
TOOL = usbasp

CFLAGS = -mmcu=$(MCU) -Wall -g -Os -Werror -lm -mcall-prologues -std=c99 -DF_CPU=$(CLK)
LFLAGS = -mmcu=$(MCU) -Wall -g -Os -Werror 

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(TGT): $(OBJ)
	$(CC) $(LFLAGS) -o $@.elf  $(OBJ) -lm
	$(OC) -O binary -R .eeprom -R .nwram $@.elf $@.bin
	$(OC) -O ihex -R .eeprom -R .nwram $@.elf $@.hex
  
clean:
	rm -f *.elf *.bin *.hex  $(OBJ) *.map

flash:
	$(AD) -p $(PART) -c $(TOOL) -U flash:w:$(TGT).hex
	
all: clean $(TGT) flash
