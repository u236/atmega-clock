#ifndef CONFIG_H
#define CONFIG_H

#define IN_18
#define START_COUNT  100
#define TIMER_COUNT  100
#define ENC_COUNT    4

#define I2C_SPEED    400000
#define I2C_BAUD     ( ( ( F_CPU /  I2C_SPEED ) - 16 ) / 2 )

#define I2C_PORT      PORTC
#define I2CSDA_PIN   _BV( PC4 )
#define I2CSCL_PIN   _BV( PC5 )

#define IN_PORT      PORTD
#define IN_PIN       PIND
#define ALARM_PIN    _BV( PD0 )
#define BTN_PIN      _BV( PD1 )
#define ENCA_PIN     _BV( PD2 )
#define ENCB_PIN     _BV( PD3 )
#define SENSOR_PIN   _BV( PD4 )
#define SIZE_PIN     _BV( PD7 )

#define HV_DDR       DDRD
#define HVPWM_PIN    _BV( PD5 )

#define BEEP_PWM     0
#define BEEP_COUNT   105
#define BEEP_DDR     DDRB
#define BEEP_PORT    PORTB
#define BEEP_PIN     _BV( PB1 )

#define SR_DDR       DDRC
#define SR_PORT      PORTC
#define SRCLK_PIN    _BV( PC2 )
#define SRLAT_PIN    _BV( PC1 )
#define SRDAT_PIN    _BV( PC0 )

#define WS2812_DDR   DDRD
#define WS2812_PORT  PORTD
#define WS2812_PIN   _BV( PD6 )

#if BEEP_PWM
#define BEEP_ON      OCR1A = 127
#define BEEP_OFF     OCR1A = 0
#else
#define BEEP_ON      BEEP_PORT |= BEEP_PIN
#define BEEP_OFF     BEEP_PORT &= ~( BEEP_PIN )
#endif

#ifdef  IN_12
#define HVDUTY       180
#define DOT_COUNT    6
#define INVERT       0
#define LED_ENABLED  0
#define LED_DIVIDER  0
#endif

#ifdef  IN_14
#define HVDUTY       210
#define DOT_COUNT    0
#define INVERT       1
#define LED_ENABLED  1
#define LED_DIVIDER  2
#endif

#ifdef  IN_18
#define HVDUTY       200
#define DOT_COUNT    0
#define INVERT       0
#define LED_ENABLED  1
#define LED_DIVIDER  1
#endif

#define COLOR_0        0,   0,   0
#define COLOR_1      160, 160, 160
#define COLOR_2       40, 160,  40
#define COLOR_3        0, 160,   0
#define COLOR_4       40, 160,   0
#define COLOR_5       80, 160,   0
#define COLOR_6      160,   0,   0
#define COLOR_7       80,   0, 160
#define COLOR_8        0,   0, 160
#define COLOR_9        0,  80, 160

enum
{
   SETUP_IDLE,
   SETUP_LED_COLOR_1,
   SETUP_LED_COLOR_2,
   SETUP_LED_COLOR_3,
   SETUP_LED_COLOR_4,
   SETUP_LED_COLOR_5,
   SETUP_LED_COLOR_6,
   SETUP_ALARM_HOUR,
   SETUP_ALARM_MINUTE,
   SETUP_CLOCK_YEAR,
   SETUP_CLOCK_MONTH,
   SETUP_CLOCK_DAY,
   SETUP_CLOCK_HOUR,
   SETUP_CLOCK_MINUTE,
   SETUP_CLOCK_SECOND
};

#endif
