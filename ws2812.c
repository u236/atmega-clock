#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "config.h"

void ws2812_write( const void *data, uint16_t size )
{
   uint8_t p_dn = WS2812_PORT & ~( WS2812_PIN );
   uint8_t p_up = WS2812_PORT | WS2812_PIN;
   uint8_t bits = 7;

   WS2812_PORT = p_dn;
   _delay_loop_1( 100 );
   cli();

   __asm volatile(
         "start:  LDI %[bits], 7             \n"
         "        LD __tmp_reg__, %a[data]+  \n"
         "cont06: NOP                        \n"
         "cont07: NOP                        \n"
         "        OUT %[port], %[dreg]       \n"
         "cont09: LSL __tmp_reg__            \n"
         "s00:    OUT %[port], %[ureg]       \n"
         "        BRCS skip03                \n"
         "        OUT %[port], %[dreg]       \n"
         "skip03: SUBI %[bits], 1            \n"
         "        BRNE cont06                \n"
         "        LSL __tmp_reg__            \n"
         "        BRCC Lx008                 \n"
         "        LDI %[bits], 7             \n"
         "        OUT %[port], %[dreg]       \n"
         "        NOP                        \n"
         "        OUT %[port], %[ureg]       \n"
         "        SBIW %[size], 1            \n"
         "        LD __tmp_reg__, %a[data]+  \n"
         "        BRNE cont07                \n"
         "        RJMP brk18                 \n"
         "Lx008:  OUT %[port], %[dreg]       \n"
         "        LDI %[bits], 7             \n"
         "        OUT %[port], %[ureg]       \n"
         "        NOP                        \n"
         "        OUT %[port], %[dreg]       \n"
         "        SBIW %[size], 1            \n"
         "        LD __tmp_reg__, %a[data]+  \n"
         "        BRNE cont09                \n"
         "brk18:  OUT %[port], %[dreg]       \n"
         ::
         [data] "e" (data),
         [ureg] "r" (p_up),
         [dreg] "r" (p_dn),
         [size] "w" (size),
         [bits] "d" (bits),
         [port] "I" (_SFR_IO_ADDR(WS2812_PORT))
   );

   sei();
}
