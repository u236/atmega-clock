#include <avr/io.h>
#include "i2c.h"

static void ds1307_read( uint8_t address, uint8_t *data )
{
   i2c_start();
   i2c_write( 0xD0 );
   i2c_write( address );
   i2c_start();
   i2c_write( 0xD1 );
   i2c_read( data, 0 );
   i2c_stop();
}

static void ds1307_write( uint8_t address, uint8_t data )
{
   i2c_start();
   i2c_write( 0xD0 );
   i2c_write( address );
   i2c_write( data );
   i2c_stop();
}

void ds1307_get_date( uint8_t *d, uint8_t *m, uint8_t *y )
{
   uint8_t data;

   ds1307_read( 0x04, &data );
   *d = ( data >> 4 ) * 10 + ( data & 0x0F );

   ds1307_read( 0x05, &data );
   *m = ( data >> 4 ) * 10 + ( data & 0x0F );

   ds1307_read( 0x06, &data );
   *y = ( data >> 4 ) * 10 + ( data & 0x0F );
}

void ds1307_get_time( uint8_t *h, uint8_t *i, uint8_t *s )
{
   uint8_t data;

   ds1307_read( 0x02, &data );
   *h = ( data >> 4 ) * 10 + ( data & 0x0F );

   ds1307_read( 0x01, &data );
   *i = ( data >> 4 ) * 10 + ( data & 0x0F );

   ds1307_read( 0x00, &data );
   *s = ( data >> 4 ) * 10 + ( data & 0x0F );
}

void ds1307_set_date( uint8_t d, uint8_t m, uint8_t y )
{
   ds1307_write( 0x04, ( ( d / 10 ) << 4 ) | d % 10 );
   ds1307_write( 0x05, ( ( m / 10 ) << 4 ) | m % 10 );
   ds1307_write( 0x06, ( ( y / 10 ) << 4 ) | y % 10 );
}

void ds1307_set_time( uint8_t h, uint8_t i, uint8_t s )
{
   ds1307_write( 0x00, ( ( s / 10 ) << 4 ) | s % 10 );
   ds1307_write( 0x01, ( ( i / 10 ) << 4 ) | i % 10 );
   ds1307_write( 0x02, ( ( h / 10 ) << 4 ) | h % 10 );
}
