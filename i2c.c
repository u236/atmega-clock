#include <avr/io.h>

void i2c_init( uint8_t baud )
{
   TWBR = baud;
   TWSR = 0;
}

void i2c_start( void )
{
   TWCR = _BV( TWINT ) | _BV( TWSTA ) | _BV( TWEN );

   while ( !( TWCR & _BV( TWINT ) ) )
   {
   }
}

void i2c_stop( void )
{
   TWCR = _BV( TWINT ) | _BV( TWSTO ) | _BV( TWEN );

   while ( TWCR & ( 1 << TWSTO ) )
   {
   }
}

void i2c_read( uint8_t *data, uint8_t ack )
{
   if ( ack )
      TWCR |= _BV( TWEA );
   else
      TWCR &= ~( _BV( TWEA ) );

   TWCR |= _BV( TWINT );

   while ( !( TWCR & _BV( TWINT ) ) )
   {
   }

   *data = TWDR;
}

void i2c_write( uint8_t data )
{
   TWDR = data;
   TWCR = _BV( TWINT ) | _BV( TWEN );

   while ( !( TWCR & _BV( TWINT ) ) )
   {
   }
}
