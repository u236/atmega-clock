#ifndef I2C_H
#define I2C_H

void i2c_init( uint8_t baud );
void i2c_start( void );
void i2c_stop( void );
void i2c_read( uint8_t *data, uint8_t ack );
void i2c_write( uint8_t data );

#endif
