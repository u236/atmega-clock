#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include "config.h"
#include "i2c.h"
#include "ds1307.h"
#include "ws2812.h"

uint8_t td, tm, ty, th, ti, ts, tah, tai, ls, beep_count, beep, fullsize = 0, dot = 0x03, digit[6], led_color[6], led_setup = 0, setup_mode = 0;
uint8_t color_table[10][3] = { { COLOR_0 }, { COLOR_1 }, { COLOR_2 }, { COLOR_3 }, { COLOR_4 }, { COLOR_5 }, { COLOR_6 }, { COLOR_7 }, { COLOR_8 }, { COLOR_9 } };
uint8_t month_days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

static void hw_init( void )
{
   TCCR0A = _BV( COM0B1 ) | _BV( WGM01 ) | _BV( WGM00 );
   TCCR0B = _BV( CS00 );

   if ( BEEP_PWM )
   {
      TCCR1A = _BV( COM0A1 ) | _BV( WGM11 ) | _BV( WGM10 );
      TCCR1B = _BV( CS10 );
   }

   TCCR2B = _BV( CS22 );
   TIMSK2 = _BV( TOIE2 );

   OCR0B = HVDUTY;

   I2C_PORT |= I2CSDA_PIN | I2CSCL_PIN;
   IN_PORT |= ALARM_PIN | BTN_PIN | ENCA_PIN | ENCB_PIN | SIZE_PIN;

   HV_DDR |= HVPWM_PIN;
   BEEP_DDR |= BEEP_PIN;
   SR_DDR |= SRCLK_PIN | SRLAT_PIN | SRDAT_PIN;
   WS2812_DDR |= WS2812_PIN;

   if ( IN_PIN & SIZE_PIN )
      fullsize = 1;

   if ( LED_ENABLED && !( IN_PIN & BTN_PIN ) )
      led_setup = 1;

   i2c_init( I2C_BAUD );
   sei();
}

static void sr_write( uint16_t data )
{
   SR_PORT &= ~( SRCLK_PIN | SRLAT_PIN | SRDAT_PIN );

   for ( uint8_t i = 0; i < 16; i++ )
   {
      SR_PORT &= ~( SRCLK_PIN );

      if ( data & ( 0x8000 >> i ) )
         SR_PORT |= SRDAT_PIN;
      else
         SR_PORT &= ~( SRDAT_PIN );

      SR_PORT |= SRCLK_PIN;
   }

   SR_PORT |= SRLAT_PIN;
}

static void cycle_number( uint8_t *data, uint8_t min, uint8_t max, uint8_t forward )
{
   if ( forward && *data < max )
      *data += 1;
   else if ( forward && *data == max )
      *data = min;
   else if ( *data > min )
      *data -= 1;
   else if ( *data == min )
      *data = max;
}

static uint8_t invert_digit( uint8_t data )
{
   return data < 2 ? ~data & 0x01 : 11 - data;
}

static void show_date( uint8_t d, uint8_t m, uint8_t y )
{
   dot = 0x02;
   digit[0] = d / 10;
   digit[1] = d % 10;
   digit[2] = m / 10;
   digit[3] = m % 10;
   digit[4] = y / 10;
   digit[5] = y % 10;
}

static void show_time( uint8_t h, uint8_t i, uint8_t s, uint8_t blink )
{
   if ( blink && ( s % 2 ) )
      dot = 0;
   else
      dot = 0x03;

   digit[0] = h / 10;
   digit[1] = h % 10;
   digit[2] = i / 10;
   digit[3] = i % 10;
   digit[4] = s / 10;
   digit[5] = s % 10;
}

static void animate_time( uint8_t h, uint8_t i, uint8_t s )
{
   for ( uint8_t j = 0; j < 6; j++ )
      digit[j] = 0;

   while ( digit[0] < h / 10 )
   {
      digit[0]++;
      _delay_ms( START_COUNT );
   }

   while ( digit[1] < h % 10 )
   {
      digit[1]++;
      _delay_ms( START_COUNT );
   }

   while ( digit[2] < i / 10 )
   {
      digit[2]++;
      _delay_ms( START_COUNT );
   }

   while ( digit[3] < i % 10 )
   {
      digit[3]++;
      _delay_ms( START_COUNT );
   }

   while ( digit[4] < s / 10 )
   {
      digit[4]++;
      _delay_ms( START_COUNT );
   }

   while ( digit[5] < s % 10 )
   {
      digit[5]++;
      _delay_ms( START_COUNT );
   }
}

static void enc_action( uint8_t forward )
{
   if ( setup_mode < SETUP_ALARM_HOUR )
      cycle_number( &led_color[setup_mode - 1], 0, 9, forward );
   else
      switch ( setup_mode )
      {
         case SETUP_ALARM_HOUR:
            cycle_number( &tah, 0, 23, forward );
            break;

         case SETUP_ALARM_MINUTE:
            cycle_number( &tai, 0, 59, forward );
            break;

         case SETUP_CLOCK_YEAR:
            cycle_number( &ty, 18, 99, forward );
            break;

         case SETUP_CLOCK_MONTH:
            cycle_number( &tm, 1, 12, forward );
            break;

         case SETUP_CLOCK_DAY:

            if ( !( ty % 4 ) && tm == 2 )
               cycle_number( &td, 1, 29, forward );
            else
               cycle_number( &td, 1, month_days[tm - 1], forward );

            break;

         case SETUP_CLOCK_HOUR:
            cycle_number( &th, 0, 23, forward );
            break;

         case SETUP_CLOCK_MINUTE:
            cycle_number( &ti, 0, 59, forward );
            break;

         case SETUP_CLOCK_SECOND:
            cycle_number( &ts, 0, 59, forward );
            break;
      }
}

static void led_update( void )
{
   static uint8_t check = 1;
   uint8_t data[18] = { 0 };

   if ( dot )
      for ( uint8_t i = 0; i < ( fullsize ? 6 : 4 ); i++ )
         for ( uint8_t j = 0; j < 3; j++ )
            if ( color_table[led_color[i]][j] )
            {
               data[i * 3 + j] = color_table[led_color[i]][j] / ( LED_DIVIDER ? LED_DIVIDER : 1 );
               check = 1;
            }

   if ( check )
   {
      ws2812_write( &data, fullsize ? 18 : 12 );

      if ( !dot )
         check = 0;
   }
}

int main( void )
{
   int8_t enc_count = 0;
   uint8_t d, m, y, h, i, s, ah, ai, alarm_event = 0, alarm_flag = 0, alarm_setup = 0, btn_last = 0, enc_last = 0;
   hw_init();

   eeprom_read_block( ( void * ) &led_color, ( const void * ) 0x00, 6 );

   for ( uint8_t i = 0; i < 6; i++ )
      if ( led_color[i] > 9 )
         led_color[i] = 4;

   ah = eeprom_read_byte( ( uint8_t * ) 0x06 );
   ai = eeprom_read_byte( ( uint8_t * ) 0x07 );

   if ( ah > 23 || ai > 59 )
   {
      ah = 0;
      ai = 0;
   }

   ds1307_get_time( &h, &i, &s );

   if ( h > 23 || i > 59 || s > 59 )
      ds1307_set_time( 12, 0, 0 );
   else
      animate_time( h, i, s );

   while ( 1 )
   {
      uint8_t input = IN_PIN, enc_data = ( input & ( ENCA_PIN | ENCB_PIN ) ) >> 2;
      ds1307_get_date( &d, &m, &y );
      ds1307_get_time( &h, &i, &s );

      if ( !( input & BTN_PIN ) && !btn_last )
      {
         setup_mode++;

         if ( !led_setup && setup_mode == SETUP_LED_COLOR_1 )
            setup_mode = SETUP_ALARM_HOUR;

         if ( setup_mode == SETUP_ALARM_HOUR )
         {
            if ( !( input & ALARM_PIN ) )
            {
               tah = ah;
               tai = ai;
               alarm_setup = 1;
            }
            else if ( fullsize )
            {
               td = d;
               tm = m;
               ty = y;
               setup_mode = SETUP_CLOCK_YEAR;
            }
            else
               setup_mode = SETUP_CLOCK_HOUR;
         }

         if ( setup_mode == SETUP_CLOCK_DAY )
         {
            if ( !( ty % 4 ) && tm == 2 && td > 29 )
               td = 29;
            else if ( td > month_days[tm - 1] )
               td = month_days[tm - 1];
         }

         if ( setup_mode == SETUP_CLOCK_HOUR )
         {
            th = h;
            ti = i;
            ts = s;
         }

         if ( ( led_setup && setup_mode > ( fullsize ? SETUP_LED_COLOR_6 : SETUP_LED_COLOR_4 ) ) || ( alarm_setup && setup_mode > SETUP_ALARM_MINUTE ) || ( setup_mode > ( fullsize ? SETUP_CLOCK_SECOND : SETUP_CLOCK_MINUTE ) ) )
         {
            if ( led_setup )
            {
               eeprom_write_block( ( const void * ) &led_color, ( void * ) 0x00, 6 );
               led_setup = 0;
            }
            else if ( alarm_setup )
            {
               eeprom_write_byte( ( uint8_t * ) 0x06, tah );
               eeprom_write_byte( ( uint8_t * ) 0x07, tai );
               ah = tah;
               ai = tai;
               alarm_setup = 0;
            }
            else
            {
               if ( fullsize )
               {
                  ds1307_set_date( td, tm, ty );
                  ds1307_set_time( th, ti, ts );
               }
               else
               {
                  ds1307_set_date( 0, 0, 0 );
                  ds1307_set_time( th, ti, 0 );
               }
            }

            setup_mode = SETUP_IDLE;
         }

         btn_last = 1;
      }
      else if ( input & BTN_PIN )
         btn_last = 0;

      if ( setup_mode )
      {
         if ( enc_data != enc_last )
         {
            if ( ( enc_last == 0 && enc_data == 1 ) || ( enc_last == 1 && enc_data == 3 ) || ( enc_last == 2 && enc_data == 0 ) || ( enc_last == 3 && enc_data == 2 ) )
            {
               if ( enc_count > 0 )
                  enc_count = 0;

               enc_count--;
            }
            else
            {
               if ( enc_count < 0 )
                  enc_count = 0;

               enc_count++;
            }

            if ( enc_count == ENC_COUNT || enc_count == -ENC_COUNT )
            {
               enc_action( enc_count == ENC_COUNT ? 1 : 0 );
               enc_count = 0;
            }

            enc_last = enc_data;
         }

         if ( led_setup )
         {
            for ( uint8_t i = 0; i < ( fullsize ? 6 : 4 ); i++ )
               digit[i] = led_color[i];
         }
         else
         {
            if ( alarm_event )
            {
               BEEP_OFF;
               alarm_event = 0;
            }

            if ( !alarm_setup )
            {
               if ( setup_mode < SETUP_CLOCK_HOUR )
                  show_date( td, tm, ty );
               else
                  show_time( th, ti, ts, 0 );
            }
            else
               show_time( tah, tai, 0, 0 );
         }
      }
      else
      {
         if ( !( input & ALARM_PIN ) )
         {
            if ( h == ah && i == ai && !s )
               alarm_event = 1;

            if ( ls != s )
            {
               beep = 1;
               beep_count = 0;
               ls = s;
            }

            if ( h != ah && i == ai )
               alarm_event = 0;

            if ( alarm_event && !( s % 2 ) && beep )
               BEEP_ON;
            else
               BEEP_OFF;
         }
         else if ( alarm_event )
         {
            BEEP_OFF;
            alarm_event = 0;
         }

         if ( !( input & SENSOR_PIN ) )
         {
            show_time( h, i, s, ( ( input & ALARM_PIN ) || alarm_event ) ? 1 : 0 );
            alarm_flag = 0;
         }
         else
         {
            if ( !alarm_event )
            {
               if ( fullsize && !alarm_flag )
                  show_date( d, m, y );
               else
                  show_time( h, i, s, ( input & ALARM_PIN ) ? 1 : 0 );
            }
            else
            {
               alarm_event = 0;
               alarm_flag = 1;
            }
         }
      }

      if ( LED_ENABLED )
         led_update();
   }
}

ISR( TIMER2_OVF_vect )
{
   static uint8_t digit_jump = 0, digit_step = 0, dot_step = 0;
   uint16_t data = 0;
   TCNT2 = TIMER_COUNT;

   beep_count++;

   if ( beep_count == BEEP_COUNT )
   {
      beep = beep ? 0 : 1;
      beep_count = 0;
   }

   if ( !digit_jump )
   {
      data = ( uint16_t ) ( ( INVERT ? invert_digit( digit[digit_step] ) : digit[digit_step] ) << 8 ) | ( 1 << digit_step );
      digit_step++;

      if ( digit_step == ( fullsize ? 6 : 4 ) )
         digit_step = 0;

      digit_jump = 1;
   }
   else
   {
      data = 0xFF00;
      digit_jump = 0;
   }

   if ( DOT_COUNT )
   {
      if ( dot_step < DOT_COUNT / 2 && ( dot & 0x01 ) )
         data |= 0x80;
      else if ( dot & 0x02 )
         data |= 0x40;

      dot_step++;

      if ( dot_step == DOT_COUNT )
         dot_step = 0;
   }

   sr_write( data );
}
