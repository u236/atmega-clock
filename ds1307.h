#ifndef DS1307_H
#define DS1307_H

void ds1307_get_date( uint8_t *d, uint8_t *m, uint8_t *y );
void ds1307_get_time( uint8_t *h, uint8_t *m, uint8_t *s );
void ds1307_set_date( uint8_t d, uint8_t m, uint8_t y );
void ds1307_set_time( uint8_t h, uint8_t m, uint8_t s );

#endif
